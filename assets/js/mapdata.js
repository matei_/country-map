var simplemaps_countrymap_mapdata  = {

    main_settings: {
        //General settings
        width: "550", //or 'responsive'
        background_color: "#FFFFFF",
        background_transparent: "yes",
        border_color: "#ffffff",
        pop_ups: "yes",

        //State defaults
        state_description: "",
        state_color: "#88A4BC",
        state_hover_color: "#3B729F",
        state_url: "",
        border_size: 0.5,
        all_states_inactive: "no",
        all_states_zoomable: "yes",


        //Location defaults
        location_description: "",
        location_url: "",
        location_color: "#FF0067",
        location_opacity: 0.8,
        location_hover_opacity: 1,
        location_size: 25,
        location_type: "square",
        location_image_source: "frog.png",
        location_border_color: "#FFFFFF",
        location_border: 2,
        location_hover_border: 2.5,
        all_locations_inactive: "no",
        all_locations_hidden: "no",

        //Label defaults
        label_color: "#d5ddec",
        label_hover_color: "#d5ddec",
        label_size: 22,
        label_font: "Arial",
        hide_labels: "no",
        hide_eastern_labels: "no",

        //Zoom settings
        zoom: "no",
        manual_zoom: "yes",
        back_image: "no",
        initial_back: "1",
        initial_zoom: "-1",
        initial_zoom_solo: "no",
        region_opacity: 1,
        region_hover_opacity: 0.6,
        zoom_out_incrementally: "yes",
        zoom_percentage: 0.99,
        zoom_time: 0.5,

        //Popup settings
        popup_color: "white",
        popup_opacity: 0.9,
        popup_shadow: 1,
        popup_corners: 5,
        popup_font: "12px/1.5 Verdana, Arial, Helvetica, sans-serif",
        popup_nocss: "no",

        //Advanced settings
        div: "map",
        auto_load: "yes",
        url_new_tab: "yes",
        images_directory: "default",
        fade_time: 0.1,
        link_text: "View Website"
    },

    state_specific: {
        DJ: {
            name: "Dolj",
            description: "default",
            color: "default", 
            hover_color: "default",
            url: "default"
            //image_url: "https://www.roedu.ro/sigle/ijfacademy.png"
        },
        GJ: {
            name: "Gorj"
        },
        MH: {
            name: "Mehedinti"
        },
        OT: {
            name: "Olt"
        },
        TR: {
            name: "Teleorman"
        },
        B: {
            name: "Bucuresti"
        },
        CL: {
            name: "Calarasi"
        },
        DB: {
            name: "Dâmbovita"
        },
        GR: {
            name: "Giurgiu"
        },
        IL: {
            name: "Ialomita"
        },
        CT: {
            name: "Constanta"
        },
        AR: {
            name: "Arad"
        },
        BH: {
            name: "Bihor"
        },
        CS: {
            name: "Caras-Severin"
        },
        TM: {
            name: "Timis"
        },
        BT: {
            name: "Botosani"
        },
        AB: {
            name: "Alba"
        },
        BN: {
            name: "Bistrita-Nasaud"
        },
        CJ: {
            name: "Cluj"
        },
        HD: {
            name: "Hunedoara"
        },
        MM: {
            name: "Maramures"
        },
        MS: {
            name: "Mures"
        },
        SJ: {
            name: "Salaj"
        },
        SM: {
            name: "Satu-Mare"
        },
        AG: {
            name: "Arges"
        },
        SB: {
            name: "Sibiu"
        },
        VL: {
            name: "Valcea"
        },
        BV: {
            name: "Brasov"
        },
        CV: {
            name: "Covasna"
        },
        HR: {
            name: "Harghita"
        },
        IS: {
            name: "Iasi"
        },
        NT: {
            name: "Neamt"
        },
        PH: {
            name: "Prahova"
        },
        SV: {
            name: "Suceava"
        },
        BC: {
            name: "Bacau"
        },
        BR: {
            name: "Braila"
        },
        BZ: {
            name: "Buzau"
        },
        GL: {
            name: "Galati"
        },
        VS: {
            name: "Vaslui"
        },
        VN: {
            name: "Vrancea"
        },
        IF: {
            name: "Ilfov"
        },
        TL: {
            name: "Tulcea"
        }
    },
};
