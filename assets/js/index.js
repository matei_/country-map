$('#closeInfo').click(function() {
    $("#infoModal" ).modal('hide');
});

$('#closeSearch').click(function() {
    $("#searchModal" ).modal('hide');
});

$(document).ready(function() {
    $(".owl-carousel").owlCarousel({
        loop: true,
        dots: false,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                nav: true
            },
            480: {
                items: 3,
                nav: true
            },
            768: {
                items: 4,
                nav: true
            }
        }
    });
});