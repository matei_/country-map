<?php

    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    require_once __DIR__ . '../../config/config.inc.php';

?>

<!-- info_modal_body -->
<div class = "modal-body">
    <?php

        $select_query = "SELECT * FROM clubajj WHERE Judet = ? ORDER BY ID";
        $judet = $_GET['q'];
    
        if($STMT = $link -> prepare($select_query)) {
            $STMT -> bind_param('s', $judet);
    
            // Execute query.
            $STMT -> execute();
    
            // Store the result (to get properties).
            $STMT -> store_result();
            
            // Get the number of rows.
            $num_rows = $STMT -> num_rows;
    
            // Bind the result to variables.
            $STMT -> bind_result($club_id, $club_name, $club_judet, $club_adresa);

            if($num_rows < 1) {
                echo '<p class = "font-weight-bold">Momentan, nu exista rezultate pentru judetul selectat.</p>';
            } else {
    ?>

    <table class = "table table-responsive table-inverse table-hover table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nume</th>
                <th>Judet</th>
                <th>Adresa</th>
            </tr>
        </thead>
        <?php while($STMT -> fetch()) {
            echo "<tr>";
            echo "<td>" .$club_id. "</td>";
            echo "<td>" .$club_name. "</td>";
            echo "<td>" .$club_judet. "</td>";
            echo "<td>" .$club_adresa. "</td>";
            echo "</tr>";
        }
        ?>
    </table>
    <?php 
            // Free results.
            $STMT -> free_result();

            // Close statement.
            $STMT -> close();
        } 
        // Close database connection.
        $link -> close();
}
?>
</div>