<?php

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    require '../vendor/autoload.php';

    $form_page = "../form.html";
    $error_page = "../error.html";
    $done_page = "../done.html";

    /*
        Loading form fields data into variables.
    */
    $club_name = $_REQUEST['club_name'];
    $club_address_1 = $_REQUEST['club_address_1'];
    $club_address_2 = $_REQUEST['club_address_2'];
    $city_name = $_REQUEST['city_name'];
    $country_name = $_REQUEST['country_name'];
    $zip_code = $_REQUEST['zip_code'];
    $phone_no = $_REQUEST['phone_no'];
    $club_email = $_REQUEST['club_email'];
    $afiliat_jrf = $_REQUEST['afiliat_jrf'];
    $afiliat_ajj = $_REQUEST['afiliat_ajj'];
    $coach_name_1 = $_REQUEST['coach_name_1'];
    $coach_name_2 = $_REQUEST['coach_name_2'];
    $coach_name_3 = $_REQUEST['coach_name_3'];
    $website = $_REQUEST['website'];
    $facebook = $_REQUEST['facebook'];
    $social_media = $_REQUEST['social_media'];

    /* Dojo 1 */
    $dojo_1_address_1 = $_REQUEST['dojo_1_address_1'];
    $dojo_1_address_2 = $_REQUEST['dojo_1_address_2'];
    $dojo1_city_name = $_REQUEST['dojo1_city_name'];
    $dojo1_country_name = $_REQUEST['dojo1_country_name'];
    $dojo1_zip = $_REQUEST['dojo1_zip'];
    $dojo1_phone_no = $_REQUEST['dojo1_phone_no'];
    $dojo1_email = $_REQUEST['dojo1_email'];
    $dojo1_tatami = $_REQUEST['dojo1_tatami'];
    $dojo1_program = $_REQUEST['dojo1_program'];

    /* Dojo 2 */
    $dojo_2_address_1 = $_REQUEST['dojo_2_address_1'];
    $dojo_2_address_2 = $_REQUEST['dojo_2_address_2'];
    $dojo2_city_name = $_REQUEST['dojo2_city_name'];
    $dojo2_country_name = $_REQUEST['dojo2_country_name'];
    $dojo2_zip = $_REQUEST['dojo2_zip'];
    $dojo2_phone_no = $_REQUEST['dojo2_phone_no'];
    $dojo2_email = $_REQUEST['dojo2_email'];
    $dojo2_tatami = $_REQUEST['dojo2_tatami'];
    $dojo2_program = $_REQUEST['dojo2_program'];

    /* Dojo 3 */
    $dojo_3_address_1 = $_REQUEST['dojo_3_address_1'];
    $dojo_3_address_2 = $_REQUEST['dojo_3_address_2'];
    $dojo3_city_name = $_REQUEST['dojo3_city_name'];
    $dojo3_country_name = $_REQUEST['dojo3_country_name'];
    $dojo3_zip = $_REQUEST['dojo3_zip'];
    $dojo3_phone_no = $_REQUEST['dojo3_phone_no'];
    $dojo3_email = $_REQUEST['dojo3_email'];
    $dojo3_tatami = $_REQUEST['dojo3_tatami'];
    $dojo3_program = $_REQUEST['dojo3_program'];

    $other_info = $_REQUEST['other_info'];

    $email_msg = nl2br( 
        "Nume asociatie / club sportiv: " .$club_name. "\r\n" .
        "Adresa sediu social: " .$club_address_1 . ", " . $club_address_2. "\r\n" .
        "Oras: " .$city_name. "\r\n" .
        "Stat / provincie: " .$country_name. "\r\n" .
        "Cod postal: " .$zip_code. "\r\n" .
        "Numar de telefon / persoana de contact / functie: " .$phone_no. "\r\n" .
        "Email: " .$club_email. "\r\n" .
        "Afiliat Federatia Romana de Judo?: " .$afiliat_jrf. "\r\n" .
        "Afiliat Asociatia Judeteana de Judo?: " .$afiliat_ajj. "\r\n" .
        "Nume antrenor 1: " .$coach_name_1. "\r\n" .
        "Nume antrenor 2: " .$coach_name_2. "\r\n" .
        "Nume antrenor 3: " .$coach_name_3. "\r\n" .
        "Adresa web: " .$website. "\r\n" .
        "Facebook: " .$facebook. "\r\n" .
        "Alt tip de cont social: " .$social_media. "\r\n" .

        /* Dojo 1 */
        "Adresa - Dojo 1: " .$dojo_1_address_1 . ", " . $dojo_1_address_2. "\r\n" .
        "Oras - Dojo 1: " .$dojo1_city_name. "\r\n" .
        "Stat / provincie - Dojo 1: " .$dojo1_country_name. "\r\n" .
        "Cod postal - Dojo 1: " .$dojo1_zip. "\r\n" .
        "Numa de telefon / persoana de contact / functie - Dojo 1: " .$dojo1_phone_no. "\r\n" .
        "Email - Dojo 1: " .$dojo1_email. "\r\n" .
        "MP de Tatami - Dojo 1: " .$dojo1_tatami. "\r\n" .
        "Grupe de varsa / program - Dojo 1: " .$dojo1_program. "\r\n" .

        /* Dojo 2 */
        "Adresa - Dojo 2: " .$dojo_2_address_1 . ", " . $dojo_2_address_2. "\r\n" .
        "Oras - Dojo 2: " .$dojo2_city_name. "\r\n" .
        "Stat / provincie - Dojo 2: " .$dojo2_country_name. "\r\n" .
        "Cod postal - Dojo 2: " .$dojo2_zip. "\r\n" .
        "Numa de telefon / persoana de contact / functie - Dojo 2: " .$dojo2_phone_no. "\r\n" .
        "Email - Dojo 2: " .$dojo2_email. "\r\n" .
        "MP de Tatami - Dojo 2: " .$dojo2_tatami. "\r\n" .
        "Grupe de varsa / program - Dojo 2: " .$dojo2_program. "\r\n" .

        /* Dojo 3 */
        "Adresa - Dojo 3: " .$dojo_3_address_1 . ", " . $dojo_3_address_2. "\r\n" .
        "Oras - Dojo 3: " .$dojo3_city_name. "\r\n" .
        "Stat / provincie - Dojo 3: " .$dojo3_country_name. "\r\n" .
        "Cod postal - Dojo 3: " .$dojo3_zip. "\r\n" .
        "Numa de telefon / persoana de contact / functie - Dojo 3: " .$dojo3_phone_no. "\r\n" .
        "Email - Dojo 3: " .$dojo3_email. "\r\n" .
        "MP de Tatami - Dojo 3: " .$dojo3_tatami. "\r\n" .
        "Grupe de varsa / program - Dojo 3: " .$dojo3_program. "\r\n" .

        "Alte detalii / informatii despre promovarea clubului: " .$other_info
    );

    // Create an instance; passing 'true' enables exceptions.
    $sendForm = new PHPMailer(true);
    try {
        // Server settings
        $sendForm -> SMTPDebug = SMTP::DEBUG_OFF;
        $sendForm -> isSMTP();
        $sendForm -> Host = 'mail.yourdomain.ro';
        $sendForm -> SMTPAuth = true;
        $sendForm -> Username = '_username';
        $sendForm -> Password = '_password';
        $sendForm -> SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
        $sendForm -> Port = 465;

        // Recipients
        $sendForm -> setFrom('no-rply@ajj.ro', 'Actualizare baza de date AJJ.RO');
        $sendForm -> addAddress('your@email.com');

        // Content
        $sendForm -> isHTML(true);
        $sendForm -> Subject = 'Actualizare / introducere in baza de date AJJ.RO - Asociatia Judeteana de Judo Romania';
        $sendForm -> Body = $email_msg;

        $sendForm -> send();
    } catch(Exception $e) {
        echo "Mesajul nu a putut fi trimis. Mailer Error: {$sendForm -> ErrorInfo}";
    }
