<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    require_once __DIR__ . '../../config/config.inc.php';

?>

<!-- search_modal_content -->
<div class = "modal-body">
    <?php 
        if(!isset($_POST['submit'])) {
            $searchValue = $_POST['search'];
    
            $search_result = mysqli_query($link, "SELECT * FROM clubajj WHERE Nume or Adresa LIKE '%$searchValue%'");
            $count = mysqli_num_rows($search_result);

            if($count < 1) {
                echo '<p class = "text-align-center font-weight-bold">Nu s-a gasit niciun rezultat care sa corespunda cu cautarea dumneavoastra.</p>';
            } else { 
    ?>
        <table class = "table table-responsive table-inverse table-hover table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nume</th>
                    <th>Judet</th>
                    <th>Adresa</th>
                </tr>
            </thead>
                <?php while($row = mysqli_fetch_assoc($search_result)) {
                    echo "<tr>";
                    echo "<td>".$row['ID']."</td>";
                    echo "<td>".$row['Nume']."</td>";
                    echo "<td>".$row['Judet']."</td>";
                    echo "<td>".$row['Adresa']."</td>";
                    echo "</tr>";
                } 
                echo "</table>";
            }
        }
        mysqli_close($link);       
    ?>
</div>